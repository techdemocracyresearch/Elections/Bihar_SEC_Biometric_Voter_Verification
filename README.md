# Bihar SEC Biometric Voter Verification


## About

This project attempts to document and understand the biometric voter verification deployed for the Bihar Panchayat Elections 2021. It aims to do so by performing reverse engineering of the multiple versions of the app used in the exercise.

- [App Binaries and Reversed Source Code](https://filen.io/f/d0d80980-618a-4b63-b927-d9259cacf967#!3nNomRe34GyYp7R1r7gAzLutzseDzxWz)
- [Aadhaar biometric based ID verification - planned and unplanned outcomes](https://hasgeek.com/onevote/electiontech/sub/aadhaar-biometric-based-id-verification-planned-an-EaVU1pQjkcYGSFfqfUV55h)
